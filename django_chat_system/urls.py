from django.conf import settings
from django.urls import re_path as url, include
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/account/', include(("users.urls", 'users-api'), namespace='users-api')),
    url(r'^api/', include(("message.urls", 'msg-api'), namespace='msg-api')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
