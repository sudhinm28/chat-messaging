from django.db import models
from django.conf import settings
import time
from django.db.models.signals import pre_save

User = settings.AUTH_USER_MODEL  # grabing the User model

def upload_location(instance, filename):
    filebase, extension = filename.split(".")
    return "images/%s.%s" %(int(round(time.time() * 1000)), extension)

def file_upload_location(instance, filename):
    filebase, extension = filename.split(".")
    return "files/%s.%s" %(int(round(time.time() * 1000)), extension)

class Message(models.Model):
    message_id = models.CharField(max_length=50, blank=True)
    sender = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="sender", on_delete = models.CASCADE)
    recipient = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="receiver", on_delete = models.CASCADE)
    subject = models.CharField(max_length=50, blank=True)
    body = models.TextField()
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    read = models.BooleanField(default=False)
    file = models.FileField(upload_to=file_upload_location,
                            null=True,
                            blank=True
                            )
    image = models.ImageField(upload_to=upload_location,
                              null=True,
                              blank=True,
                              )
    objects = models.Manager()

    def __str__(self):
        return str(self.subject)




# class Recipient(models.Model):
#     user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="+", on_delete=models.CASCADE)
#     message = models.ForeignKey(Message, related_name="+", on_delete=models.CASCADE)
#     read = models.BooleanField(default=False)
#     # invite_reason = models.CharField(max_length=64)
#     objects = models.Manager()
