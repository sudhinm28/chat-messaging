from django.urls import re_path as url
from django.contrib import admin

from .views import (
    MsgCreateAPIView,
    SentMsgListAPIView,
    InboxMsgListAPIView,
    MsgDeleteAPIView,
    MsgReadAPIView

)

urlpatterns = [
    url(r'^message/create$', MsgCreateAPIView.as_view(), name='msg-create'),
    url(r'^message/sent/$', SentMsgListAPIView.as_view(), name='sent-msg-list'),
    url(r'^message/inbox/$', InboxMsgListAPIView.as_view(), name='receive-msg-list'),
    url(r'^message/read/(?P<id>\d+)/$', MsgReadAPIView.as_view(), name='msg-read'),
    url(r'^message/delete/(?P<id>\d+)/$', MsgDeleteAPIView.as_view(), name='msg-delete'),
]