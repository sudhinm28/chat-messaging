from rest_framework.serializers import (
    HyperlinkedIdentityField,
    ModelSerializer,
    SerializerMethodField,
    ValidationError,
    Serializer,
    FileField,
    IntegerField
)
import time
from drf_extra_fields.fields import Base64ImageField

from message.models import Message
from django.contrib.auth import get_user_model
User = get_user_model()


class MsgCreateSerializer(ModelSerializer):
    image = Base64ImageField(required=False)
    class Meta(object):
        model = Message
        fields = [
            'id',
            'message_id',
            'sender',
            'recipient',
            'subject',
            'body',
            'timestamp',
            'file',
            'image'
        ]



msg_read_url = HyperlinkedIdentityField(
        view_name='msg-api:msg-read',
        lookup_field='id'
    )

msg_delete_url = HyperlinkedIdentityField(
        view_name='msg-api:msg-delete',
        lookup_field='pk'
    )

msg_archive_url = HyperlinkedIdentityField(
        view_name='msg-api:msg-archived',
        lookup_field='pk'
    )


class SentMsgListSerializer(ModelSerializer):
    # timestamp = SerializerMethodField()
    # sender = UserDetailSerializer(read_only=True)
    # recipient = UserDetailSerializer(read_only=True)
    class Meta(object):
        model = Message
        fields = [
            'id',
            'message_id',
            'sender',
            'subject',
            'body',
            'recipient',
            'image',
            'file',
            'timestamp',
        ]

    def get_timestamp(self, object):
        return object.timestamp.strftime("%A %B %d, %Y   %I:%M%p %Z")


class InboxMsgListSerializer(ModelSerializer):
    # timestamp = SerializerMethodField()
    # sender = UserDetailSerializer(read_only=True)
    # recipient = UserDetailSerializer(read_only=True)
    class Meta(object):
        model = Message
        fields = [
            'id',
            'message_id',
            'sender',
            'subject',
            'body',
            'recipient',
            'image',
            'file',
            'timestamp',
            'read',
        ]

    def get_timestamp(self, object):
        return object.timestamp.strftime("%A %B %d, %Y   %I:%M%p %Z")


class MessageDetailSerializer(ModelSerializer):
    class Meta(object):
        model = Message
        fields = [
            'id',
            'timestamp',
        ]
