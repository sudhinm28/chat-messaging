from rest_framework.response import Response
from django.http import JsonResponse
import time
import uuid
from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    ListAPIView, 
    UpdateAPIView,
    ListCreateAPIView
)
from .pagination import MsgPageNumberPagination
from django.db.models import Q
from django.contrib.auth import get_user_model
from message.models import Message
from .serializers import (
    MsgCreateSerializer,
    SentMsgListSerializer,
    InboxMsgListSerializer,
    MessageDetailSerializer,

)

User = get_user_model()


class MsgCreateAPIView(CreateAPIView):
    serializer_class = MsgCreateSerializer

    def post(self, request, *args, **kwargs):
        # import pdb;pdb.set_trace()
        required_fields = ["recipient", "sender", "subject", "body"]
        for i in required_fields:
            if not i in request.data.keys():
                return Response({i: "This field is required."})

        message_id = str(uuid.uuid1().time)
        if request.data.get('recipient'):
            recipient = request.data.get('recipient')
        else:
            return Response({"id": ["This field is required."]})
        if isinstance(recipient, int):
            request.data['recipient'] = recipient
            request.data['message_id'] = message_id
            serializer = MsgCreateSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
            else:
                return Response(serializer.errors)
        else:
            for i in recipient:
                request.data['recipient'] = i
                request.data['message_id'] = message_id
                serializer = MsgCreateSerializer(data=request.data)
                if serializer.is_valid():
                    serializer.save()
                else:
                    return Response(serializer.errors)
        return Response({"msg": "Successfully sent the message"})


class SentMsgListAPIView(ListAPIView):
    serializer_class = SentMsgListSerializer
    # pagination_class = MsgPageNumberPagination

    def get_queryset(self, *args, **kwargs):
        query = self.request.GET.get("id")
        # queryset = Message.objects.filter(sender=query).distinct('recipient')
        queryset = Message.objects.filter(sender=query)
        serializer = SentMsgListSerializer(data=queryset, many=True)
        serializer.is_valid()
        return queryset


class InboxMsgListAPIView(ListAPIView):
    serializer_class = InboxMsgListSerializer
    # pagination_class = MsgPageNumberPagination

    def get_queryset(self, *args, **kwargs):
        query = self.request.GET.get("id")
        # queryset = Message.objects.filter(sender=query).distinct('recipient')
        queryset = Message.objects.filter(recipient=query)
        serializer = InboxMsgListSerializer(data=queryset, many=True)
        serializer.is_valid()
        return queryset


class MsgReadAPIView(UpdateAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageDetailSerializer
    lookup_field = 'id'

    def put(self, request, *args, **kwargs):
        read_value = request.data['read']
        instance = Message.objects.filter(id=kwargs['id']).update(read=read_value)
        return JsonResponse({'msg': 'Successfully read message'})


class MsgDeleteAPIView(DestroyAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageDetailSerializer
    lookup_field = 'id'

    def delete(self, request, *args, **kwargs):
        instance = Message.objects.filter(id=kwargs['id']).delete()
        return JsonResponse({'msg': 'Successfully delete message'})




