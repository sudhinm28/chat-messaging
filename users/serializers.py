from django.db.models import Q
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
# from .models import Profile

from rest_framework.serializers import (
    EmailField,
    HyperlinkedIdentityField,
    ModelSerializer,
    SerializerMethodField,
    ValidationError,
    CharField,
    Serializer
)
User = get_user_model()

class UserDetailSerializer(ModelSerializer):
    class Meta(object):
        model = User
        fields = [
            'id',
            'username',
            'email',
            'imageurl',
            'firstname',
            'lastname',
        ]
            
class UserListSerializer(ModelSerializer):
    class Meta(object):
        model = User
        fields = [
            'id',
            'username',
            'email',
            'firstname',
            'lastname',
            'imageurl',
        ]

        def create(self, validated_data):
            return User.objects.create(**validated_data)

class UserCreateSerializer(ModelSerializer):
    email = EmailField(label='Email Address')
    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password',

        ]
        extra_kwargs = {"password": {"write_only": True }}
    def validate(self, data):
        email = data['email']
        user_qs = User.objects.filter(email=email)
        if user_qs.exists():
            raise ValidationError("This user has already registered.")
        return data
    
    def create(self, validated_data):
        username = validated_data['username']
        email = validated_data['email']
        password = validated_data['password']

        user_obj = User(
                username=username,
                email=email
            )
        user_obj.set_password(password)
        user_obj.save()
        return validated_data


class RegistrationSerializer(ModelSerializer):
    confirm_password = CharField(style={"input_type": "password"}, write_only=True)

    class Meta:
        model = User
        fields = [
            'email',
            'password',
            'confirm_password',
            'firstname',
            'lastname',
            'imageurl',
            'username']
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def save(self):
        user = User(email=self.validated_data['email'],
                    firstname=self.validated_data['firstname'],
                    lastname=self.validated_data['lastname'],
                    imageurl=self.validated_data.get('imageurl', None),
                    username=self.validated_data.get('username', None))
        password = self.validated_data['password']
        confirm_password = self.validated_data['confirm_password']
        if password != confirm_password:
            raise ValidationError({'password': 'Passwords must match.'})
        user.set_password(password)
        user.save()
        return user


class PasswordChangeSerializer(Serializer):
    current_password = CharField(style={"input_type": "password"}, required=True)
    new_password = CharField(style={"input_type": "password"}, required=True)

    def validate_current_password(self, value):
        if not self.context['request'].user.check_password(value):
            raise ValidationError({'current_password': 'Does not match'})
        return value