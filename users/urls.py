from django.urls import re_path as url
from django.contrib import admin
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView
from .views import (
    UserListAPIView,
    SearchUserView,
    ChangePasswordView,
    LoginView,
    LogoutView,
    RegistrationView,
    UserAPIView,
    UserUpdateView
)

urlpatterns = [
    # url(r'^register/$', UserCreateAPIView.as_view(), name='register'),
    url(r'^register/$', RegistrationView.as_view(), name='register'),
    url(r'^update/', UserUpdateView.as_view(), name='update'),
    url(r'^change_password/$', ChangePasswordView.as_view(), name='register'),
    url(r'^login/', TokenObtainPairView.as_view(), name = 'login'),
    url(r'^token-verify/', TokenVerifyView.as_view(), name= 'token'),
    # url(r'^login/', LoginView.as_view(), name='login'),
    url(r'^logout/', LogoutView.as_view(), name='logout'),
    url(r'^user/', UserAPIView.as_view(), name='user'),
    url(r'^list_user/', UserListAPIView.as_view(), name='user-list'),
    url(r'^search/user/$', SearchUserView.as_view(), name='search-user-list'),
]