from django.db.models import Q
from django.contrib.auth import get_user_model
# from .models import Profile
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth import authenticate, login, logout
from .utils import get_tokens_for_user
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView
from rest_framework.authentication import SessionAuthentication, BasicAuthentication

from rest_framework.generics import (
    CreateAPIView,
    ListAPIView,
    UpdateAPIView
)

from rest_framework.permissions import (
    AllowAny,
)

User = get_user_model()

from .serializers import (
    UserCreateSerializer,
    UserListSerializer,
    RegistrationSerializer,
    PasswordChangeSerializer,
)


class UserListAPIView(ListAPIView):
    serializer_class = UserListSerializer
    def get_queryset(self, *args, **kwargs):
        if self.request.GET.get("id"):
            queryset_list = User.objects.exclude(id = self.request.GET.get("id"))
        else:
            queryset_list = User.objects.all()
        return queryset_list


class UserAPIView(ListAPIView):
    serializer_class = UserListSerializer
    def get_queryset(self, *args, **kwargs):
        id = self.request.GET.get("id")
        queryset_list = User.objects.filter(id = id)
        return queryset_list


class SearchUserView(ListAPIView):

    serializer_class = UserListSerializer
    def get_queryset(self, *args, **kwargs):
           # result = super(SearchUserView, self).get_queryset()
           search_query = self.request.GET.get('search')
           id = self.request.GET.get('id')
           if search_query:
               queryset_list = User.objects.filter(frstname__contains=search_query).exclude(id=id)
               return queryset_list
           else:
               return []


class RegistrationView(APIView):
    def post(self, request):
        serializer = RegistrationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            queryset = User.objects.filter(email = request.data["email"]).values('id')
            user_dict = {"id": queryset[0]['id']}
            user_dict.update(serializer.data)
            return Response(user_dict, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LoginView(APIView):
    def post(self, request):
        if 'email' not in request.data or 'password' not in request.data:
            return Response({'msg': 'Credentials missing'}, status=status.HTTP_400_BAD_REQUEST)
        email = request.data['email']
        password = request.data['password']
        user = authenticate(request, email=email, password=password)
        if user is not None:
            login(request, user)
            auth_data = get_tokens_for_user(request.user)
            return Response({'msg': 'Login Success', **auth_data}, status=status.HTTP_200_OK)
        return Response({'msg': 'Invalid Credentials'}, status=status.HTTP_401_UNAUTHORIZED)


class LogoutView(APIView):
    # permission_classes = [IsAuthenticated]
    def post(self, request):
        logout(request)
        return Response({'msg': 'Successfully Logged out'}, status=status.HTTP_200_OK)


class ChangePasswordView(APIView):
    # permission_classes = [IsAuthenticated]

    def post(self, request):
        serializer = PasswordChangeSerializer(context={'request': request}, data=request.data)
        serializer.is_valid(raise_exception=True)  # Another way to write is as in Line 17
        request.user.set_password(serializer.validated_data['new_password'])
        request.user.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

class UserUpdateView(UpdateAPIView):
    # permission_classes = [IsAuthenticated]
    queryset = User.objects.all()
    # serializer_class = UserCreateSerializer
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def put(self, request, *args, **kwargs):
        data = request.data
        wanted_keys = ['id', 'email', 'username', 'firstname', 'lastname', 'imageurl']
        new_dict = {k: data[k] for k in set(wanted_keys) & set(data.keys())}
        if 'email' in new_dict.keys():
            queryset = User.objects.filter(email=new_dict['email'])
            if queryset.exists():
                return Response({'msg': 'email already exist'})
        if 'username' in new_dict.keys():
            queryset = User.objects.filter(username=new_dict['username'])
            if queryset.exists():
                return Response({'msg': 'username already exist'})
        # for key, value in new_dict.items():
        instance = User.objects.filter(id=new_dict['id']).update(**new_dict)
        return Response({'msg': 'successfully updated'})


