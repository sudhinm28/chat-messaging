from django.apps import AppConfig


class AccountsApiConfig(AppConfig):
    name = 'users'
